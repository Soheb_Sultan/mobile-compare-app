var compare = {
    object1: {
        _id: "http://www.gsmarena.com/htc_one-5313.php",
        title: "HTC One",
        link: "http://www.gsmarena.com/htc_one-5313.php",
        imagePath: "http://cdn2.gsmarena.com/vv/bigpic/htc-one-m7-new1.jpg",
        "2G Network": "GSM 850 / 900 / 1800 / 1900 ",
        "3G Network": "HSDPA 850 / 900 / 1900 / 2100 ",
        "4G Network": "LTE 800 / 1800 / 2600  LTE 1800 / 2600LTE 1900 - for SprintLTE 700 / 850 / 1700 / 1900 / 2100 - for AT&TLTE 700 / 1700 / 2100 - for T-Mobile",
        SIM: "Micro-SIM",
        Announced: "2013, February",
        Status: "Available. Released 2013, March",
        Dimensions: "137.4 x 68.2 x 9.3 mm (5.41 x 2.69 x 0.37 in)",
        Weight: "143 g (5.04 oz)",
        Type: "Super LCD3 capacitive touchscreen, 16M colors",
        // Size: "1080 x 1920 pixels, 4.7 inches (~469 ppi pixel density)",
        Size: "768 x 1280 pixels, 4.7 inches (~318 ppi pixel density)",
        Multitouch: "Yes",
        Protection: "Corning Gorilla Glass 2",
        "Addon Display": "- HTC Sense UI v5",
        "Alert types": "Vibration, MP3, WAV ringtones",
        Loudspeaker: "\r\nVoice 69dB / Noise 66dB / Ring 75dB",
        "Addon Sound": "- Beats Audio sound enhancement",
        "Card slot": "No",
        Internal: "32/64 GB, 2 GB RAM",
        GPRS: "Yes",
        EDGE: "Yes",
        Speed: "HSPA+; LTE, Cat3, 50 Mbps UL, 100 Mbps DL",
        WLAN: "Wi-Fi 802.11 a/b/g/n/ac, Wi-Fi Direct, DLNA, Wi-Fi hotspot",
        Bluetooth: "Yes, v4.0 with A2DP",
        NFC: "Yes (Market dependent)",
        "Infrared port": "Yes",
        USB: "Yes, microUSB v2.0 (MHL), USB On-the-go, USB Host",
        Primary: "4 MP, 2688 x 1520 pixels, autofocus, optical image stabilization, LED flash, check quality",
        Features: "1/3'' sensor size, 2�m pixel size, simultaneous HD video and image recording, geo-tagging, face and smile detection",
        Video: "Yes, 1080p@30fps, 720p@60fps, HDR, stereo sound rec., video stabilization, check quality",
        Secondary: "Yes, 2.1 MP, 1080p@30fps, HDR",
        // Secondary: "Yes, VGA",
        OS: "Android OS, v4.1.2 (Jelly Bean), v4.3 (Jelly Bean), planned upgrade to v4.4 (KitKat)",
        Chipset: "Qualcomm APQ8064T Snapdragon 600",
        CPU: "Quad-core 1.7 GHz Krait 300",
        GPU: "Adreno 320",
        Sensors: "Accelerometer, gyro, proximity, compass",
        Messaging: "SMS (threaded view), MMS, Email, Push Email",
        Browser: "HTML5",
        Radio: "Stereo FM radio with RDS",
        GPS: "Yes, with A-GPS support and GLONASS",
        Java: "Yes, via Java MIDP emulator",
        Colors: "Black, Silver, Red, Blue, Gold",
        "Addon Features": "- SNS integration\r\n- Dropbox (25 GB cloud storage)\r\n- Active noise cancellation with dedicated mic\r\n- TV-out (via MHL A/V link)\r\n- DivX/XviD/MP4/H.263/H.264/WMV player\r\n- MP3/eAAC+/WMA/WAV/FLAC player\r\n- Google Search, Maps, Gmail,\r\nYouTube, Calendar, Google Talk\r\n- Organizer\r\n- Document viewer/editor\r\n- Photo viewer/editor\r\n- Voice memo/dial/commands\r\n- Predictive text input",
        "Addon Battery": "Non-removable Li-Po 2300 mAh battery",
        "Stand-by": "Up to 500 h (2G) / Up to 480 h (3G)",
        "Talk time": "Up to 27 h (2G) / Up to 18 h (3G)",
        "SAR US": "\r\n0.66 W/kg (head)     0.40 W/kg (body)     ",
        "SAR EU": "\r\n0.86 W/kg (head)     0.36 W/kg (body)     ",
        "Price group": "",
        Display: "\r\nContrast ratio: 1541:1 (nominal) / 2.504:1 (sunlight)",
        "Audio quality": "\r\nNoise -92.4dB / Crosstalk -92.4dB",
        Camera: "\r\nPhoto / Video",
        "Battery life": "\r\nEndurance rating 48h",
        "Addon Tests": ""
    },

    object2: {
        _id: "http://www.gsmarena.com/samsung_i9295_galaxy_s4_active-5446.php",
        title: "Samsung I9295 Galaxy S4 Active",
        link: "http://www.gsmarena.com/samsung_i9295_galaxy_s4_active-5446.php",
        imagePath: "http://cdn2.gsmarena.com/vv/bigpic/samsung-i9295-galaxy-s4-active-ofic.jpg",
        "2G Network": "GSM 850 / 900 / 1800 / 1900 ",
        "3G Network": "HSDPA 850 / 900 / 1900 / 2100 HSDPA 850 / 1900 / 2100 ",
        "4G Network": "LTE 800 / 850 / 900 / 1800 / 2100 / 2600 LTE 700 / 850 / 1700 / 1900 / 2100 / 2600 - SGH-I537",
        SIM: "Micro-SIM",
        Announced: "2013, June",
        Status: "Available. Released 2013, June",
        Dimensions: "139.7 x 71.3 x 9.1 mm (5.5 x 2.81 x 0.36 in)",
        Weight: "153 g (5.40 oz)",
        "Addon Body": "- IP67 certified - dust and water resistant\r\n- Water resistant up to 1 meter and 30 minutes",
        Type: "TFT capacitive touchscreen, 16M colors",
        // Size: "1080 x 1920 pixels, 5.0 inches (~441 ppi pixel density)",
        Size: "1080 x 1920 pixels, 4.95 inches (~445 ppi pixel density)",
        Multitouch: "Yes",
        Protection: "Corning Gorilla Glass 2",
        "Addon Display": "- TouchWiz UI\r\n",
        "Alert types": "Vibration; MP3, WAV ringtones",
        Loudspeaker: "\r\nVoice 72dB / Noise 66dB / Ring 78dB",
        "Card slot": "microSD, up to 64 GB",
        Internal: "16 GB (11.2 GB user available), 2 GB RAM",
        GPRS: "Class 33",
        EDGE: "Class 33",
        Speed: "HSDPA, 42 Mbps; HSUPA, 5.76 Mbps; LTE, Cat3, 50 Mbps UL, 100 Mbps DL",
        WLAN: "Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, DLNA, Wi-Fi hotspot",
        Bluetooth: "Yes, v4.0 with A2DP, EDR, LE",
        NFC: "Yes",
        "Infrared port": "Yes",
        USB: "Yes, microUSB v2.0 (MHL 2), USB On-the-go, USB Host",
        Primary: "13 MP, 4128 x 3096 pixels, autofocus, LED flash",
        Features: "Simultaneous HD video and image recording, geo-tagging, touch focus, face and smile detection, image stabilization, HDR, Aqua mode",
        Video: "Yes, 1080p@30fps, check quality",
        Secondary: "Yes, 2 MP,1080p@30fps",
        OS: "Android OS, v4.2.2 (Jelly Bean)",
        Chipset: "Qualcomm APQ8064T Snapdragon 600",
        CPU: "Quad-core 1.9 GHz Krait 300",
        GPU: "Adreno 320",
        Sensors: "Accelerometer, gyro, proximity, compass, barometer, gesture",
        Messaging: "SMS(threaded view), MMS, Email, Push Mail, IM, RSS",
        Browser: "HTML5",
        Radio: "No",
        GPS: "Yes, with A-GPS support and GLONASS",
        Java: "Yes, via Java MIDP emulator",
        Colors: "Urban Grey, Dive Blue, Orange Flare",
        "Addon Features": "- S-Voice natural language commands and dictation\r\n- Smart stay, Smart pause, Smart scroll\r\n- Air gestures\r\n- Dropbox (50 GB cloud storage)\r\n- Active noise cancellation with dedicated mic\r\n- TV-out (via MHL 2 A/V link)\r\n- SNS integration\r\n- MP4/DivX/XviD/WMV/H.264/H.263 player\r\n- MP3/WAV/eAAC+/AC3/FLAC player\r\n- Organizer\r\n- Image/video editor\r\n- Document viewer (Word, Excel, PowerPoint, PDF)\r\n- Google Search, Maps, Gmail,\r\nYouTube, Calendar, Google Talk, Picasa\r\n- Voice memo/dial/commands\r\n- Predictive text input (Swype)",
        "Addon Battery": "Li-Ion 2600 mAh battery",
        "Stand-by": "Up to 312 h",
        "Talk time": "Up to 17 h",
        "SAR US": "\r\n0.55 W/kg (head)     0.75 W/kg (body)     ",
        "SAR EU": "\r\n0.26 W/kg (head)     0.36 W/kg (body)     ",
        "Price group": "",
        Display: "\r\nContrast ratio: 1046:1 (nominal), 2.022 (sunlight)",
        "Audio quality": "\r\nNoise -96.1dB / Crosstalk -96.0dB",
        Camera: "\r\nPhoto / Video",
        "Battery life": "\r\nEndurance rating 57h",
        "Addon Tests": ""
    },

    extract_memory_size: function(number) {
        if(typeof(this["object"+number]["Internal"]) !== 'undefined') {
            var internal_size = this["object"+number]["Internal"];
            var size = 0;
            var length = internal_size.length;
            // Iterating into memory field string
            for (var i = 0; i < length; i++) {
              if(internal_size[i] == "G" && internal_size[i+1] == "B") {
                size = parseInt(internal_size);
                size *= 1024;
                return size;
              }
              else if(internal_size[i] == "M" && internal_size[i+1] == "B") {
                size = parseInt(internal_size);
                return size;
              }
            }
        }
        return 0;
    },

    memory_size: function(number) {
        var size = this.extract_memory_size(number);
        if(size > 0){
            var songs = parseInt(size/6);
            return "Can store ~" + Math.ceil(songs/100)*100 + " songs";
        }
        return "Not available";
    },

    memory_size_comparison: function() {
        var phone_one = this.extract_memory_size(1);
        var phone_two = this.extract_memory_size(2);

        if(phone_one == 0 && phone_two == 0)
            return "Memory field not available";
        else if(phone_one > phone_two)
          return this.object1["title"] + " has more memory";
        else if(phone_two > phone_one)
          return this.object2["title"] + " has more memory";
        else return "Both phones have same memory";
    },

    extract_external_size: function(number) {
        if(typeof(this["object"+number]["Card slot"]) !== 'undefined') {
            var external = this["object"+number]["Card slot"];
            var regx = /\d+\s[a-zA-Z]*/;
            if (external.length != 2) {
              var size_arr = external.match(regx);
              return parseInt(size_arr[0]);
            }
            else return 0;
        }
        else return -1;
    },

    external_size: function(number) {
        var size = this.extract_external_size(number);
        if(size == -1)
            return "Not available";
        else if(size > 0){
            var size_str = this["object"+number]["Card slot"].match(/\d+\s[a-zA-Z]*/);
            return "Upto " + size_str[0];
        }
        else return "No card slot support";
    },

    external_size_comparison: function() {
        var phone_one = this.extract_external_size(1);
        var phone_two = this.extract_external_size(2);
        
        if(phone_one == -1 && phone_two == -1)
            return "External memory field not available";
        else if(phone_one > phone_two)
            return this.object1["title"] + " has more external memory support";
        else if(phone_two > phone_one)
            return this.object2["title"] + " has more external memory support";
        else return "Both phones have same external memory"; 
    },
    
    extract_back_camera: function(number) {
        if(typeof(this["object"+number]["Primary"]) !== 'undefined') {
            var back_camera = this["object"+number]["Primary"];
                if(back_camera.match(/pixels/) !== null) {
                    var regex = /\d+(\s)?(x|х)(\s)?\d+/;
                    var pixels_arr=back_camera.match(regex);
                    var pixels= pixels_arr[0].split(/x|х/);
                    return pixels[0] * pixels[1];
                }
                else return 0;
        }
        return 0;
    },

    back_camera_comparison: function() {
        var phone_one = this.extract_back_camera(1);
        var phone_two = this.extract_back_camera(2);
        var result = 0;
        if (phone_one == 0 && phone_two !== 0) {
            return this.object2["title"] + " will zoom upto 100 percent more without degrading picture quality";
        }
        else if (phone_two == 0 && phone_one !== 0) {
            return this.object1["title"] + " will zoom upto 100 percent more without degrading picture quality";
        }
        else if (phone_one > phone_two){
          result = Math.round(((phone_one - phone_two)/phone_two)*100);
          return this.object1["title"] + " will zoom upto " + result + " percent more without degrading picture quality";
        } 
        else if (phone_two > phone_one){
          result = Math.round(((phone_two - phone_one)/phone_one)*100);
         return this.object2["title"] + " will zoom upto " + result + " percent more without degrading picture quality";
        } 
        else return "Both phones have same pixels";
    },

    extract_front_camera: function(number) {
        if(typeof(this["object"+number]["Secondary"]) !== 'undefined') {
            var camera = this["object"+number]["Secondary"];
            if(camera.match(/yes/i) !== null) {
                if(camera.match(/(\d.+MP)/) !== null) {
                    for(var i = 0;i < camera.length;i++){
                        var pixels_arr = camera.match(/(\d.+MP)/);
                        return parseFloat(pixels_arr[0]);
                    }
                }
                return -5; // incase of YES
            }
            return -4; // incase of NO
        }
        return -3; // incase of no field available
    },

    front_camera: function(number) {
        var pixels = this.extract_front_camera(number);
        if(pixels == -5)
            return "Yes";
        else if(pixels == -4)
            return "No front camera";
        else if(pixels > 0)
            return pixels + " MP";
        else return "Camera field not available";
    },  

    front_camera_comparison: function() {
        var phone_one = this.front_camera(1);
        var phone_two = this.front_camera(2);

        if(phone_one > phone_two)
          return this.object1["title"] + " has better front camera";
        else if(phone_two > phone_one)
          return this.object2["title"] + " has better front camera";
        else return "Both phones have same front camera";
    },

    extract_os_version: function(number) {
        if(typeof(this["object"+number]["OS"]) !== 'undefined') {
            var string = this["object"+number]["OS"];
            var regex = /(\d.)+/;
            var os_version = string.match(regex);
            return os_version[0];
        }
        return 0;
    },

    os_version_comparison: function() {
        var phone_one = this.extract_os_version(1);
        var phone_two = this.extract_os_version(2);

        var os_one = phone_one.split('.');
        var os_two = phone_two.split('.');

        for(var i = 0; i <= 1; i++){
            if(os_one[i] > os_two[i])
                return this.object1["title"] + " has newer OS version";
            else if(os_one[i] < os_two[i])
                return this.object2["title"] + " has newer OS version";
        }
        return "Both phones have same OS versions";
    },

    extract_screen_quality: function(number) {
        if(typeof(this["object"+number]["Size"]) !== 'undefined') {
            var size_string = this["object"+number]["Size"];
            var pixels = size_string.match(/\d+\sx\s\d+/);
            var pixels_arr = pixels[0].split(/x/);
            return pixels_arr[0] * pixels_arr[1];
        }
        return 0;
    },

    screen_quality_comparison: function(number) {
        var phone_one = this.extract_screen_quality(1);
        var phone_two = this.extract_screen_quality(2);
        var percent = 0;
        
        if(phone_two > phone_one) {
          percent = ((phone_two - phone_one)/phone_one)*100;
          return this.object2["title"] + " has " + Math.round(percent) +" percent more pixels";
        }
        else if(phone_one > phone_two) {
          percent = ((phone_one - phone_two)/phone_two)*100;
          console.log(percent);
          return this.object1["title"] + " has " + Math.round(percent) +" percent more pixels";
        }
        else return "Both phones have same screen quality";
    },

    extract_screen_size: function(number) {
        if(typeof(this["object"+number]["Size"]) !== 'undefined') {
            var size_string = this["object"+number]["Size"];
            var inches = 0; 
            var screen_size_arr = size_string.split(' ');
            var regex = /\d+\.\d+ inches/;
            for (var i = 0; i < screen_size_arr.length; i++) {
              if (screen_size_arr[i] == "inches") {
                var screenOne = size_string.match(regex);
                inches = parseFloat(screenOne[0]);
                return inches;
              }
            }
        }
        return 0;
    },

    screen_size: function(number) {
        var size = this.extract_screen_size(number);
        if (size > 4.7)
          return "Perfect for large hands";
        else if (size <= 4.7 && size > 4.3)
          return "Perfect for medium hands";
        else if (size <= 4.3)
          return "Perfect for small hands";
    },

    screen_size_comparison: function() {
        var phone_one = this.extract_screen_size(1);
        var phone_two = this.extract_screen_size(2);

        if (phone_one > phone_two)
            return this.object1["title"] + " has bigger screen";
        else if(phone_two > phone_one)
            return this.object2["title"] + " has bigger screen";
        else return "Both phones have same screen size";
    },

    extract_battery: function(number) {
        if(this["object"+number].hasOwnProperty('Battery life')){
            var battery_string = this["object"+number]["Battery life"];
            var regex = /\d+/;
            var hours = parseInt(battery_string.match(regex));
            return hours;
        }
        return 0;
    },

    battery: function(number){
        var battery = this.extract_battery(number);
        if(battery > 0)
            return battery + " hours";
        else return "No battery life available";
    },

    battery_comparison: function() {
        var phone_one = this.extract_battery(1);
        var phone_two = this.extract_battery(2);

        if(phone_one == 0 && phone_two == 0)
            return "Battery field not available";
        else if(phone_one > phone_two)
            return this.object1["title"] + " has better battery backup";
        else if(phone_two > phone_one)
            return this.object2["title"] + " has better battery backup";
        else return "Both phones have same battery life";
    },

    extract_weight: function(number) {
        if(typeof(this["object"+number]["Weight"]) !== 'undefined') {
            var weight_string = this["object"+number]["Weight"];
            for (var i = 0; i < weight_string.length; i++) {
              if (weight_string[i] == "g")
                return parseInt(weight_string);
            }
        }
        return 0;
    },

    weight: function(number) {
        var weight = this.extract_weight(number);
        if(weight > 0)
            return weight + " grams";
        else return "No weight available";
    },

    weight_comparison: function() {
        var phone_one = this.extract_weight(1);
        var phone_two = this.extract_weight(2);

        if(phone_one > phone_two)
            return this.object2["title"] + " is lighter in weight";
        else if(phone_two > phone_one)
            return this.object1["title"] + " is lighter in weight";
        else return "Both phones have same weight";
    },

    extract_network_speed: function(number) {
        if(typeof this["object"+number]["4G Network"] !== "undefined")
            return "42 Mbps";
        else if(typeof this["object"+number]["3G Network"] !== "undefined")
            return "10 Mbps";
        else return "2G";
    },

    network_speed: function(number){
        var speed = this.extract_network_speed(number);
        if(speed = '42 Mbps')
          return "Upto 42 Mbps";
        else if(speed = '10 Mbps')
          return "Upto 10 Mbps"; 
        else return "Only supports 2G";
    },

    network_speed_comparison: function() {
        // for phone 1
        var networkPhoneOne = "";
        var is4gEnabledOne = (typeof this.object1["4G Network"] !== "undefined") ? true: false;
        var is3gEnabledOne = (typeof this.object1["3G Network"] !== "undefined") ? true: false;
        var is2gEnabledOne = (typeof this.object1["2G Network"] !== "undefined") ? true: false;

        if(is4gEnabledOne)
          networkPhoneOne = 4;
        else if(is3gEnabledOne)
          networkPhoneOne = 3; 
        else networkPhoneOne = 2;

        // for phone 2
        var networkPhoneTwo = "";
        var is4gEnabledTwo = (typeof this.object2["4G Network"] !== "undefined") ? true: false;
        var is3gEnabledTwo = (typeof this.object2["3G Network"] !== "undefined") ? true: false;
        var is2gEnabledTwo = (typeof this.object2["2G Network"] !== "undefined") ? true: false;

        if(is4gEnabledTwo)
          networkPhoneTwo = 4;
        else if(is3gEnabledTwo)
          networkPhoneTwo = 3;
        else networkPhoneTwo = 2;

        if(networkPhoneOne > networkPhoneTwo)
            return this.object1["title"] + " has better internet connectivity";
        else if(networkPhoneTwo > networkPhoneOne)
            return this.object2["title"] + " has better internet connectivity";
        else return "Both phones have same internet connectivity speed";
    },

    comparePhone : function() {

        object1 = this.object1;
        object2 = this.object2;

        var return_object = {
          phone1: {
            memory : compare.memory_size(1),
            external : compare.external_size(1),
            camera : compare.front_camera(1),
            screenSize: compare.screen_size(1),
            battery : compare.battery(1),
            weight : compare.weight(1),
            network : compare.network_speed(1)
          },
          phone2: {
            memory : compare.memory_size(2),
            external : compare.external_size(2),
            camera : compare.front_camera(2),
            screenSize: compare.screen_size(2),
            battery : compare.battery(2),
            weight : compare.weight(2),
            network : compare.network_speed(2)
          },
          result: {
            backCamera : compare.back_camera_comparison(),
            OSversion : compare.os_version_comparison(),
            screenQuality : compare.screen_quality_comparison(),
            screenSize : compare.screen_size_comparison(),
            memory: compare.memory_size_comparison(),
            external: compare.external_size_comparison(),
            frontCamera : compare.front_camera_comparison(),
            battery : compare.battery_comparison(),
            weight : compare.weight_comparison(),
            network : compare.network_speed_comparison()
          }
        };
        return return_object;
    }
};

